Mobile Groomer User Documentation
=======

- [Mobile Groomer User Documentation](#mobile-groomer-user-documentation)
    - [Introduction](#introduction)
    - [First Time Setup - Page 1](#first-time-setup---page-1)
    - [First Time Setup - Page 2](#first-time-setup---page-2)
        - [Application Defaults](#application-defaults)
    - [Important First Steps](#important-first-steps)
        - [Create Services](#create-services)
        - [Create Customers & Pets](#create-customers-pets)
        - [Create Groomers](#create-groomers)
    - [Home](#home)
        - [Viewing Appointments](#viewing-appointments)
        - [Day View](#day-view)
        - [Week View](#week-view)
        - [Month View](#month-view)
    - [Adding Appointments](#adding-appointments)
    - [Check-In Menu](#check-in-menu)
    - [In-Progress Menu](#in-progress-menu)
    - [Printing](#printing)
            - [Check-In List](#check-in-list)
            - [Appointments](#appointments)
            - [Changing an Appointment](#changing-an-appointment)
    - [Configurable Settings](#configurable-settings)
        - [5 Day Work Week](#5-day-work-week)
        - [Default Calendar](#default-calendar)
        - [Timeslot (minutes)](#timeslot-minutes)
        - [Working Hours](#working-hours)
        - [Lunch Hours](#lunch-hours)
        - [Groomer Settings](#groomer-settings)
            - [Salon Groomer vs Mobile Groomer](#salon-groomer-vs-mobile-groomer)
            - [Groomer Selection Arrival or Booking](#groomer-selection-arrival-or-booking)
    - [Exporting/Importing Data](#exportingimporting-data)
        - [Exporting Your Data](#exporting-your-data)
        - [Importing Your Data](#importing-your-data)

## Introduction

Mobile Groomer is an iOS application that is geared toward Pet groomers that need a tool for tracking appointments and customer information.   The basic application will provide scheduling, groomer management, appointment scheduling and customer information management.  The basic application will be a one-time set price with potential add ons to enhance the basic application.  A Pro version is under heavy development and will provide enhanced features for a monthly fee.  

## First Time Setup - Page 1

When starting the application for the first time users will be presented with a setup wizard to guide them through a few setup questions to get the application up and going.  If you are restoring your database from an old backup or a previous device, then please skip to the "Restoring your data" section of the document.

The initial page will allow you to select the type of grooming business that you are running.   There are two available options for this selection.  

Mobile Groomers are groomers who primarily operate in a mobile unit where the location of appointments may change from one day to the next.  If you select this option, then you will be required to enter a location when booking appointments.

Salon Groomers are groomers who primarily operate in a single physical location where the location of appointments do not change.   If you select this option, then you will **NOT** be prompted to enter location information when booking appointments.   

![Image](images/FirstTimeSetupType.png)

Before continuing to the second setup step you will need to select whether your grooming business assigns groomers at booking or at arrival.   If you select Arrival, then groomers will not be assigned when appointments are booked and will be required during check-in.  If you select Booking, then a groomer must be selected at the time of booking (creating an appointment).

![Image](images/FirstTimeSetupGroomer.png)

## First Time Setup - Page 2

The second page of the first time setup wizard will help setup the calendar behavior for your business.  This includes settings such as a lunch time schedule block and working hours.

Lunch time hours effectively blocks the time period across the week to prevent accidental scheduling of appointments during the normal lunch hours for your business.  If you do not have a set time for lunch, then feel free to leave the option unchanged.

Working hours will create a boundary time for available appointment scheduling.   This prevents employees from booking appointments outside of normal business hours.

![Image](images/FirstTimeSetup2CalendarSettings.png)

### Application Defaults

Some options are not shown within the first time wizard and can be configured via the settings menu.  To access the settings menu select the Menu option at the top left of the application and then Settings from the available options.  Here are a few defaulted values that can be changed if you would prefer an alternate behavior.

Defaults:
  * Schedule View - Week is the default, but Day and Month views are available
  * Number of Days in Work Week - All 7 days are shown by default, you can alter the calendar to only show a 5 day work week by selecting "5 Day Work Week" within settings.
  * Appointment Time slot - By default the application will use 30 minute blocks of time for appointments.  You can adjust this to 60 minutes or 90 minutes if you would like.

All settings configured in the firs time wizard can be reconfigured within the settings menu if needed.  **NOTE: This will not update existing appointments if changed.**

![Image](images/Settings.png)

## Important First Steps

A few key steps are required to be completed prior to booking your first appointment.  Please complete all of the steps below.

### Create Services

Services are tasks that you will perform during an appointment for a pet.  Examples would include a bath, brushing, nail trim, etc.  These services need to be configured within the application, so that they are available for selection when booking an appointment.   Appointments require at least one service in order to complete the booking.

To add a service navigate to the Menu > Services option.

![Image](images/ServicesList.png)

To add a new service to the list, simply click the "Add" button at the top right of the application.

In the Add Service menu you will be prompted for a service code and a description.   The Service Code is considered a internal representation of the service.  For example you may refer to a blueberry scrub bath as a BB.  The application will track service codes and may expose these in the future for reporting purposes.   The description field will be leveraged on all interfaces and print-outs, so make sure and be descriptive in the description field.  Click Save once you have completed the service code and description fields.

![Image](images/AddService.png)

Repeat these steps until you have a basic number of services available for your business.

### Create Customers & Pets

Now we will create customers to use within Mobile Groomer.  Navigate to Menu > Customers.  The customers view loads with a list of the current customers within the database.  The contacts are sorted in alphabetic order.

![Image](images/CustomerList.png)

Customers can be created manually or imported from your iOS contacts.  To add a customer manually simply click the "Add" menu option at the top right.   If you would rather import one or more contacts from your iOS contacts, then select the "Import" menu option at the top right of the screen.

When selecting to import contacts you will be presented with a list of all your contacts from your default contact provider registered within iOS.  Contact entries will be presented in alphabetic order by last name.   Tap to select one or more contacts.  When finished select Import at the top right.  Contacts will be added to the application database and then will be available for adding pets.  Name, Address, Email and Phone numbers will be imported into the MobileGroomer Customer database.

![Image](images/ImportContact.png)

### Create Groomers

Groomers are employees that will complete a given appointment.   Currently Groomers are used for two primary reasons within the application. 

First and foremost, if you assign groomers at the time of booking colors are used to denote which appointments are assigned to each groomer.   This allows for complex grooming shops to see the schedule of a given Groomer at a glance.   Colors are assigned within the Groomer's profile.   **NOTE: If you change the color of a groomer the application will not currently update previous appointments to reflect the new color.**

Secondly, the MobileGroomer is tracking the groomer to allow for historical reference.  In the future we will be introducing add-ons to allow for additional reporting with this data or through the upcoming MobileGroomerPro application.

Groomers can be added to the MobileGroomer application either manually or by importing existing iOS contacts from your phone.  To add a groomer manually simply click the "Add" menu option at the top right.   If you would rather import one or more contacts from your iOS contacts, then select the "Import" menu option at the top right of the screen.  Importing a contact will configure the Groomer's Name, Phone Numbers, Email and address.   **NOTE: A default color of Orange will be assigned for the appointment color.  You will need to adjust this manually if desired.**

![Image](images/ImportContact-Groomer.png)

## Home

The home screen provides a calendar view into the current schedule of appointments.   This view will load the last **90 days of appointments**.  The view has three main options for viewing the data.   These include a Day, Week and Month view.   By default MobileGroomer is configured to show a week view.  You can temporarily change the view mode by selecting "View" from the upper right corner.   The default view can be altered via the Settings menu if you would rather see a day or month view on the Home screen.   Additionally, you can also change the week view to show either a 5 or 7 day work week.  The default is to show all 7 days in the week view.

### Viewing Appointments

Single taps will open the details of a given appointment.   A long hold (press and hold) will delete the appointment.

### Day View

Day view shows a single day and the associated appointments.  If multiple appointments are scheduled within the same slot, then they will be stacked vertically within the time slot.

![Image](images/DayView.png)

### Week View

The week view shows all appointments for a given week.  Single tap will open the details for the appointment.   A long tab (press and hold) will allow you to delete the appointment.

![Image](images/WeekView.png) 

### Month View

The month view will show all appointments for a given month.  Dots on the calendar will highlight days that have appointments.  Clicking on a day with a dot will provide a pop-up day view.   This view allows for you to find open slots within a month very quickly.  Appointments can be selected in the pop-up day list for a selected day.

![Image](images/MonthView.png)

## Adding Appointments

There are two ways to add appointments within the MobileGroomer application.  Tapping on an available slot within the Home menu is probably the most straight forward and easiest approach.  You can also click the "Add" menu option at the top right of the Home screen.

**If you need to add a second appointment for a given appointment slot, then you will need to use the "Add" menu option at the top right.**   Tapping to schedule the second appointment won't work, because the application will think you are wanting to see the details of the appointment already in that slot.

## Check-In Menu

The Check-In menu will show the current day's appointments that are pending for check-in.   This is a time bounded vew that will only show appointments in the current day that are pending check-in.  When a customer arrives for an appointment you can simply check them in within this menu.  Swiping left on the list of appointments will allow you to either check-in or view the details of the appointment.   Tapping on an appointment will also open the details view for the appointment.  A check-in button is available within the details view also.

If you have the application configured to assign groomers at check-in, then the application will prompt you to select one of the currently available groomers.   If you are assigning groomers at booking, then you will **not** be prompted to assign a groomer.


List of appointments available for Check-In:

![Image](images/CheckIn.png)


Swiping left on the list gives you these options:

![Image](images/CheckIn-Swipe.png)


## In-Progress Menu

The In-Progress menu will show the current day's appointments that are currently being worked.  It's important to mark the appointment as complete once the appointment is finished.  This will allow you to track in the future how long appointments are taking.   Reporting enhancements will be introduced in the future to allow more visibility for trending how long appointments are taking and for comparison between one groomer and another.

To mark an appointment complete swipe left and select "Complete".   Tapping on the appointment in the list or selecting the "details" option from the swipe menu will open up a detailed view for the appointment.

List of appointments available for completion:

![Image](images/InProgress.png)

Swiping left on the list gives you these options:

![Image](images/InProgress-Swipe.png)

## Printing

Basic printing functionality is currently available in the MobileGroomer application within the Check-In List and individual appointments.  Printing uses the native iOS printing feature, so any printers supported by iOS (WiFi printers) should work.

#### Check-In List

The check-in list currently offers the ability to print the current day's appointments.   The application will attempt to print 3 appointment records on a single 8"x11" sheet of paper.  The thought is that you would print these at the beginning of the day and have them available as dogs are checked in.

The Print option is in the upper right corner of the Check-In menu:

![Image](images/CheckIn.png)

#### Appointments

The appointment detail view also allows for printing of single appointments.   This can be done by opening the appointment from any of the following locations.  The print option is located at the top right of the appointment view.

* Home Screen - Tap on the appointment
* Check-In List - Select one of the appointments from the list
* In-Progress List - Select one of the appointments from the list
* Customer Detail Upcoming or Past Appointments - Select one from either list
* Groomer Upcoming Appointment List - Select one of the appointments from the list

![Image](images/AppointmentDetail.png)

#### Changing an Appointment

If you need to alter something about the appointment, then you can simply do this within the Appointment Detail view.  You may change the appointment time, services and notes for the appointment.   You cannot change the customer or pet.   If you need to change the customer or pet, then you must delete the appointment and re-book.

![Image](images/AppointmentDetails-Editable.png)

## Configurable Settings

MobileGroomer offers settings to allow for customization of the application.  These settings can be found under the Menu > Settings screen.  If you alter one of the settings, please make sure to select the "save" button at the top right of the screen to save your changes.

### 5 Day Work Week

By default MobileGroomer will display a 7 day work week.  Some groomers may only work 5 days.   This setting if enabled would hide Saturday and Sunday from the calendar views.

### Default Calendar

The default calendar setting will control what calendar view is shown on the Home screen.  The default is to show the "week" view.   However, if you would like to default to either a day or month view you can use this setting to do so.

### Timeslot (minutes)

The time slot setting will allow you to customize the default time slot value for appointments.  MobileGroomer selects 30 minutes by default, but supports the use of 30, 60 or 90 minute time slots for appointments.   When adding appointments the default timeslot time will be used to calculate the finish time for the appointment.   You can always manually adjust this within the individual appointment when your booking the appointment by changing the end time of the appointment.

### Working Hours 

Working hours affect the calendar view and ability to schedule appointments.   **Appointments cannot be booked outside of your declared working hours within the application.**   Additionally within the calendar view working hours will show as white slots and any hours outside of working hours will be greyed out to indicate those are unavailable.

### Lunch Hours

The lunch hours setting allows you to block a specific period of time as lunch time each day on the calendar.   **When this is set appointments will not be available during the lunch hour timeframe.**  By default MobileGroomer does not set lunch hours.

![Image](images/LunchHours.png)

### Groomer Settings

Groomer settings allow for you to alter the selections that were picked during the initial setup of the application.

#### Salon Groomer vs Mobile Groomer

Salon Groomer assumes that you are operating out of a single physical location.  When Salon Groomer is selected, no location information is required when booking an appointment.   If you select Mobile Groomer, then the location information will be requried when booking an appointment.

#### Groomer Selection Arrival or Booking

When set to groomer selected at arrival, groomer assignments won't be required until check-in.   If you choose groomer selected at booking, then a groomer will be required at the time of booking.

## Exporting/Importing Data

MobileGroomer uses a local database in this version of the application.   **Data will not be synced automatically.**  In the upcoming pro version of the application we will provide syncing and cloud related features.   Therefore, it is **up to you as a user** to ensure your data is backed up periodically.

### Exporting Your Data

MobileGroomer offers an export data button within the Menu -> Settings menu.   This is located at the bottom of the settings screen.  When you click the export data button it will take a snapshot of your application data and export it as a file.   This file can be saved in your iCloud files, attached to an email, Dropbox, Google Drive, etc.   Basically you can do whatever you wish with the file to complete your backup.   

![Image](images/ExportData.png)


### Importing Your Data

To import your data you can use the "share" button from the application that you saved the file to.  In most iOS applications you should be able to use the "share" button to select the file and share it back to the MobileGroomer application to import it.

![Image](images/Share.png)

Once you select the share button with the backup file that you created you should see MobileGroomer as an available application to send the file to.  In this example I used my iCloud files application as the backup location.

Select and Share:

![Image](images/ImportShareTo.png)

Select MobileGroomer as the application to copy the file to.   This should trigger the Import function within the application.

![Image](images/ImportToMobileGroomer.png)

Once you have shared the backup file back to the MobileGroomer application for import you will land on the "import" screen within the MobileGroomer application.   Click the "Import" button in the middle of the screen.   This will replace the data within the application with the backup file.  This means that you will go back to the exact data that you had when you completed the backup.  Anything within the application will be **deleted** that was done between the backup and the time you do the import.

![Image](images/MobileGroomerImportScreen.png)

After you click the import button you will see a popup indicating the import was successful.  You will now need to close the application to ensure things are restarted properly and reloaded from the phone's memory.  To do this, double-tap the home button and close the MobileGroomer application by swiping up.   Re-open the application and you should see the data as of the time you did the last backup.

![Image](images/MobileGroomerImportSuccessful.png)
